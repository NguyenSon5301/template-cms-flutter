import 'package:flutter/material.dart';

import '../../responsive.dart';

class TestPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const TestPage({super.key, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Responsive.isMobile(context) ? 15 : 18),
                child: const Center(
                  child: Text("Template Management Page"),
                ))));
  }
}
