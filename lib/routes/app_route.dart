import 'package:auto_route/auto_route.dart';

import '../screen/for_got_password_screen.dart';
import '../screen/home_screen.dart';

part 'app_route.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: HomeRoute.page, initial: true),
        AutoRoute(
            page: ForGotPasswordRoute.page,
            path: '/${ForGotPasswordRoute.name}')
      ];
}
