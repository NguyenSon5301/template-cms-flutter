import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class ReloadPostGlobal {
  ReloadPostGlobal._();
  static final _ins = ReloadPostGlobal._();
  static ReloadPostGlobal get ins => _ins;
  late TabController tabController;
  late BehaviorSubject<int> reloadPostAfterCreating$;
  void close() {
    reloadPostAfterCreating$.close();
  }
}
