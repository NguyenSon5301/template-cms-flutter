import 'package:flutter/material.dart';
import 'package:flutter_web/pages/user/user_page.dart';
import 'package:flutter_web/responsive.dart';
import 'package:flutter_web/singleton.dart';
import 'package:flutter_web/test23.dart';
import 'package:flutter_web/widgets/menu.dart';
import 'package:rxdart/subjects.dart';

import 'pages/chart/chart_page.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  List<Widget> listWidget = [];
  @override
  void initState() {
    super.initState();
    ReloadPostGlobal.ins.reloadPostAfterCreating$ =
        BehaviorSubject<int>.seeded(0);
    listWidget = [
      UserPage(
        scaffoldKey: _scaffoldKey,
      ),
      ChartPage(scaffoldKey: _scaffoldKey),
      TestPage(
        scaffoldKey: _scaffoldKey,
      ),
    ];
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    ReloadPostGlobal.ins.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: !Responsive.isDesktop(context)
            ? SizedBox(width: 250, child: Menu(scaffoldKey: _scaffoldKey))
            : null,
        // endDrawer: Responsive.isMobile(context)
        //     ? SizedBox(
        //         width: MediaQuery.of(context).size.width * 0.8,
        //         child: const Profile())
        //     : null,
        body: SafeArea(
          child: Row(
            children: [
              if (Responsive.isDesktop(context))
                Expanded(
                  flex: 2,
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Menu(scaffoldKey: _scaffoldKey)),
                ),
              Expanded(
                flex: 10,
                child: StreamBuilder(
                  stream: ReloadPostGlobal.ins.reloadPostAfterCreating$.stream,
                  builder: (context, snapshot) {
                    int list = snapshot.data ?? 0;

                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          listWidget[list],
                        ],
                      ),
                    );
                  },
                ),
              ),
              // Expanded(flex: 10, child: ChartPage(scaffoldKey: _scaffoldKey)),
              // if (!Responsive.isMobile(context))
              //   const Expanded(
              //     flex: 4,
              //     child: Profile(),
              //   ),
            ],
          ),
        ));
  }
}
