import 'package:flutter/material.dart';
import 'package:flutter_web/pages/chart/widgets/activity_details_card.dart';
import 'package:flutter_web/pages/chart/widgets/bar_graph_card.dart';
import 'package:flutter_web/pages/chart/widgets/header_widget.dart';
import 'package:flutter_web/pages/chart/widgets/line_chart_card.dart';
import 'package:flutter_web/pages/chart/widgets/liner_chart_sample_1.dart';
import 'package:flutter_web/pages/chart/widgets/pie_chart.dart';

import '../../responsive.dart';

class ChartPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const ChartPage({super.key, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              Header(scaffoldKey: scaffoldKey, title: 'Chart Page'),
              _height(context),
              const ActivityDetailsCard(),
              _height(context),
              LineChartCard(),
              _height(context),
              BarGraphCard(),
              _height(context),
              const LineChartSample1(),
              _height(context),
              const PieChartSample1(),
              _height(context),
            ],
          ),
        )));
  }
}
