import 'package:flutter/material.dart';

import '../../../responsive.dart';

class Header extends StatelessWidget {
  const Header({
    super.key,
    required this.scaffoldKey,
    required this.title,
    this.userMane = false,
  });
  final String title;
  final bool userMane;
  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (!Responsive.isDesktop(context))
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: InkWell(
                onTap: () => scaffoldKey.currentState!.openDrawer(),
                child: const Padding(
                  padding: EdgeInsets.all(3.0),
                  child: Icon(
                    Icons.menu,
                    color: Colors.grey,
                    size: 25,
                  ),
                ),
              ),
            ),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          if (!Responsive.isMobile(context)) ...[
            if (userMane) ...[
              _customButton('Add New User', Icons.add),
              SizedBox(
                width: Responsive.isDesktop(context) ? 20 : 10,
              ),
            ]
          ]
          // if (!Responsive.isMobile(context))
          //   Expanded(
          //     flex: 4,
          //     child: TextField(
          //       decoration: InputDecoration(
          //           filled: true,
          //           fillColor: cardBackgroundColor,
          //           enabledBorder: const OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.transparent),
          //           ),
          //           border: OutlineInputBorder(
          //             borderRadius: BorderRadius.circular(12.0),
          //           ),
          //           focusedBorder: OutlineInputBorder(
          //             borderRadius: BorderRadius.circular(12.0),
          //             borderSide:
          //                 BorderSide(color: Theme.of(context).primaryColor),
          //           ),
          //           contentPadding: const EdgeInsets.symmetric(
          //             vertical: 5,
          //           ),
          //           hintText: 'Search',
          //           prefixIcon: const Icon(
          //             Icons.search,
          //             color: Colors.grey,
          //             size: 21,
          //           )),
          //     ),
          //   ),
          // if (Responsive.isMobile(context))
          //   Row(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       IconButton(
          //         icon: const Icon(
          //           Icons.search,
          //           color: Colors.grey,
          //           size: 25,
          //         ),
          //         onPressed: () {},
          //       ),
          //       InkWell(
          //         onTap: () => scaffoldKey.currentState!.openEndDrawer(),
          //         child: CircleAvatar(
          //           backgroundColor: Colors.transparent,
          //           child: Image.asset(
          //             "assets/images/avatar.png",
          //             width: 32,
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
        ],
      ),
    );
  }

  Widget _customButton(String title, IconData icon) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          borderRadius: BorderRadius.circular(30),
          splashColor: Colors.blue.withOpacity(0.1),
          hoverColor: Colors.blueAccent.withOpacity(0.1),
          focusColor: Colors.green,
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: BoxDecoration(
                border:
                    Border.all(color: Colors.grey.withOpacity(0.3), width: 1.5),
                borderRadius: BorderRadius.circular(30)),
            child: Center(
              child: Row(
                children: [
                  Text(
                    title,
                    style: const TextStyle(color: Colors.white),
                  ),
                  const SizedBox(
                    width: 3,
                  ),
                  Icon(
                    icon,
                    color: Colors.blueAccent,
                  )
                ],
              ),
            ),
          )),
    );
  }
}
