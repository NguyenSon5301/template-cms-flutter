import 'package:flutter/material.dart';

import 'package:flutter_web/pages/chart/widgets/header_widget.dart';
import 'package:flutter_web/pages/user/widgets/table_data.dart';

import '../../responsive.dart';

class UserPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const UserPage({super.key, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              Header(
                scaffoldKey: scaffoldKey,
                title: 'User Management',
                userMane: true,
              ),
              _height(context),
              const TableWidget(),
              _height(context),
            ],
          ),
        )));
  }
}
