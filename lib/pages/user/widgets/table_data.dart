import 'package:flutter/material.dart';
import 'package:flutter_web/pages/user/widgets/table.dart';
import 'package:flutter_web/responsive.dart';

import '../../../widgets/custom_card.dart';

class TableWidget extends StatefulWidget {
  const TableWidget({super.key});

  @override
  State<TableWidget> createState() => _TableWidgetState();
}

class _TableWidgetState extends State<TableWidget> {
  SizedBox _height(BuildContext context) => SizedBox(
        height: Responsive.isDesktop(context) ? 30 : 20,
      );
  SizedBox _width(BuildContext context) => SizedBox(
        width: Responsive.isDesktop(context) ? 20 : 10,
      );
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return CustomCard(
        color: AppColors.contentColorWhite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  width: 300,
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: TextField(
                      onChanged: (v) {
                        setState(() {});
                      },
                      style: const TextStyle(color: Colors.black),
                      controller: _searchController,
                      decoration: InputDecoration(
                          prefixIcon: const Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          // iconColor: Colors.grey,
                          suffixIcon: _searchController.text.isNotEmpty
                              ? IconButton(
                                  icon: const Icon(
                                    Icons.clear,
                                    color: Colors.redAccent,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _searchController.text = '';
                                    });
                                  },
                                )
                              : null,
                          hintText: 'Search...',
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: InputBorder.none),
                    ),
                  ),
                ),
                if (!Responsive.isMobile(context)) ...[
                  _width(context),
                  _customButton('Filter', Icons.filter_alt_outlined),
                  _width(context),
                  _customButton(
                      'Role Type', Icons.supervised_user_circle_outlined),
                  _width(context),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                        borderRadius: BorderRadius.circular(120),
                        splashColor: Colors.blue.withOpacity(0.1),
                        hoverColor: Colors.blueAccent.withOpacity(0.1),
                        focusColor: Colors.green,
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey.withOpacity(0.3),
                                  width: 1.5),
                              borderRadius: BorderRadius.circular(120)),
                          child: const Center(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.replay_rounded,
                                  color: Colors.blueAccent,
                                )
                              ],
                            ),
                          ),
                        )),
                  ),
                ]
              ],
            ),
            _height(context),
            if (Responsive.isMobile(context)) ...[
              Row(
                children: [
                  _width(context),
                  _customButton('Filter', Icons.filter_alt_outlined),
                  _width(context),
                  _customButton(
                      'Role Type', Icons.supervised_user_circle_outlined),
                  _width(context),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                        borderRadius: BorderRadius.circular(120),
                        splashColor: Colors.blue.withOpacity(0.1),
                        hoverColor: Colors.blueAccent.withOpacity(0.1),
                        focusColor: Colors.green,
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey.withOpacity(0.3),
                                  width: 1.5),
                              borderRadius: BorderRadius.circular(120)),
                          child: const Center(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.replay_rounded,
                                  color: Colors.blueAccent,
                                )
                              ],
                            ),
                          ),
                        )),
                  ),
                ],
              ),
              _height(context)
            ],
            SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: const PaginatedDataTable2Demo()),
          ],
        ));
  }

  Widget _customButton(String title, IconData icon) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          borderRadius: BorderRadius.circular(30),
          splashColor: Colors.blue.withOpacity(0.1),
          hoverColor: Colors.blueAccent.withOpacity(0.1),
          focusColor: Colors.green,
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: BoxDecoration(
                border:
                    Border.all(color: Colors.grey.withOpacity(0.3), width: 1.5),
                borderRadius: BorderRadius.circular(30)),
            child: Center(
              child: Row(
                children: [
                  Text(
                    title,
                    style: const TextStyle(color: Colors.blueAccent),
                  ),
                  const SizedBox(
                    width: 3,
                  ),
                  Icon(
                    icon,
                    color: Colors.blueAccent,
                  )
                ],
              ),
            ),
          )),
    );
  }
}
